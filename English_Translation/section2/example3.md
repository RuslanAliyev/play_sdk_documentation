#Player#

######Public######

| Method | Parameters | Return | Description |
| -- | -- | -- | -- |
| hasDisplayLog |  | Boolean | Set whether or not to enable log to display information ( true: enable / false: disbale ) |
| isTestEnvironment |  | Boolean | Set whether or not it's testing environment ( true: testing environment / false: not testing environment ) |
| getGame |  | String | Get game's name |
| getGameVersion |  | String | get game's version |
| getServerID |  | String | Get game server's ID |
| getConnectDomain |  | String | Get server-connection URL | 
| getPaymentDomain |  | String | Get payment server's URL |
| isOpendSDK |  | Boolean | Set whether or not to enable PlaySDK's interactive hover button |
| getServiceUrl |  | String | Get service URL |
| getGiftUrl |  | String | Get the gift website's URL |
| getPayUrl |  | String | Get the URL where prepaid users' credentials are stored |															
| isOpendAngel |  | Boolean | Whether or not to enable the 'angel' icon |
| getAngelPic |  | String | Get the image of the 'angel' |
| getAngelUrl |  | String | Get the website of the 'angel' |
| isSDKFadeOut |  | Boolean | Set whether or not to enable SDK's fading effect |
| getCircleX |  | Int | Get SDK's X-coordinate |
| getCircleY |  | Int | Get SDK's Y-coordinate|
| getPlaySDKCircleText |  | String | Get PlaySDK's interactive hover button's display text |
| getGCMRegID |  | String | Get GCM Register ID |
| getDisplayGameName |  | String | Get alert window's title |
| getDisplayGameIcon |  | Int | Get alert window's icon |
| getGCMIntent() |  | String | Get the page where users get directed to after pressing Google Cloud Messaging's notification |
| displayInfo() |  | Using Log to display all information about user |