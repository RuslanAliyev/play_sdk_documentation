#Purchase Control#

Initialize Google payment mechanism (onCreate)

```

playSDK.purchaseControl().init(Activity);

```

Payment execution

```

playSDK.purchaseControl().purchaseFlow(Activity, "Product-ID", new PurchaseResponseObserver() {

  @Override
  public void onError(String msg, int event) {
    // TODO Auto-generated method stub
    // Handle purchase failure
  }

  @Override
  public void onSuccess(String msg, int event) {
    // TODO Auto-generated method stub
    // Handle purchase success
  }

});

```

Configure Google payment mechanism's callback mechanism (onActivityResult)

```

if (!playSDK.purchaseControl().getHasActivityResult(requestCode, resultCode, data)) {
  super.onActivityResult(requestCode, resultCode, data);
} else {

}

```

Terminating Google's payment mechanism (onDestroy)

```
playSDK.purchaseControl().dispose();

```