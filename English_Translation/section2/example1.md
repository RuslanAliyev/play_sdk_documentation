#Config#

######Public######

| Method | Parameters | Return | Description |
| -- | -- | -- | -- |
| hasDisplayLog | Boolean | void | Enable logging or not ( true: enable log messages / false: disable log messages ) |
| isTestEnvironment | Boolean | void | Seting whether it's testing environment or not ( true: is testing environment / false: not testing environment )|
| setConnectDomain | String, String | void | Setting the server-connection URL. 1st parameter is offical environment server. 2nd parameter is testing environment server |
| setPaymentDomain | String, String | void | Setting the payment server URL. 1st parameter is offical environment server. 2nd parameter is testing environment server |
| setGame | String | void | Set the game's name |
| setGameVersion | String | void | Set the game's version |
| setServerID | String | void | Set game server's ID number |
| setConnectPublicKey | String | void | Set server-connection public key |
| setPayPublicKey | String | void | Set payment server public key |
| setPurchasePublickey | String	| void| Set Google payment key |
| setCircleX | Int | void | Set interactive hover button's initial X coordinate |
| setCircleY | Int | void | Set interactive hover button's initial Y coordinate |
| setPlaySDKCircleText | String | void | Set interactive hover button's display text |
| setSDKFadeOut | Boolean | void | Set whether or not to enable SDK's interactive hover button's auto-fade effect (true:enable auto-fade 2 seconds after appearing/false:auto-fade only when pressed) |
| setSDKLandscape | Boolean | void | Set whether the SDK's page displays horizontally or vertically (true:horizontally/false:vertically) |
| setThirdPartyPay | Boolean | void | Set whether or not to persist prepaid users' ability to get their third party credentials when they enter a game. (true: persist their credentials when they enter game/false:start them off based on SDK's information) |
| setDisplayGameName | String | void | Set the prompt-window's title | 
| setDisplayGameIcon | int | void | Set the prompt-window's icon |
| setGCMIntent | String  |void | Set to which page the user gets directed to upon clicking the Google Cloud Messaging's notifications (usually this is your app's main page)  | 
| doClearAll |  | void | delete all information |
