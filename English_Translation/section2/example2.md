#Player#

######Public######

| Method | Parameters | Return | Description |
| -- | -- | -- | -- |
| getLoginType |  | int | Get login type. (6001: EVENT_LOGIN_JOIN. 6002:EVENT_LOGIN_BIND. 6003:EVENT_LOGIN_LOGIN) |
| getUID |  | String | Get user ID |
| getDeviceID | String |  | Get user's device ID |
| getGoogleID |  | String | Get user's Google ID |
| getFacebookID |  | String | Get user's Facebook ID |
| getServerID |  | String | Get user's server ID |
| getBindUserfrom | int |  | Get the user's binding mode |
| displayInfo() |  | void | Using Log to display all information about user |