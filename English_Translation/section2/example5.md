#GCM#

######Public######

| Method | Parameters | Return | Description |
| -- | -- | -- | -- |
| doGCMRegister |  | void | Register Google Cloud Messaging information |