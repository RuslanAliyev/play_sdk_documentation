#Register#

Play SDK have 4 registration methods:

1. Fast Registration : Fast registration via unique device ID
2. Facebook Registration : Registration via Facebook 
3. Google Registration : Registration via Google 
4. Play9388 Registration : Registration via Play9388 Email 

##Fast registration via unique device ID##

This method is for first-timers or users without accounts.

```

playSDK.api().doFastRegister(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Facebook Registration##

```

playSDK.api().doFacebookRegister(Activity, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Google Registration##

```

playSDK.api().doGoogleRegister(ConnectionResult, GoogleApiClient, Activity, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Play9388 Registration##

```

playSDK.api().doPlay9388Register(Response, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```