#Login#

Play SDK have 4 login methods:

1. Fast Login : Fast login via unique device ID
2. Facebook Login : Login via Facebook 
3. Google Login : Login via Google 
4. Play9388 Login : Login via Play9388 Email 

##Fast Login##

This method is for first-timers or for users who want to start playing without verification.

```

playSDK.api().doFastLogin(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Facebook Login##

```

playSDK.api().doFacebookLogin(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Google Login##

```

playSDK.api().doGoogleLogin(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Play9388 Login##

```

playSDK.api().doPlay9388Login(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```