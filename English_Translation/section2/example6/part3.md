#Play#

There are 4 accounts that users can use to enter Play: 

1. Fast Play : Fast login via unique device ID
2. Facebook Play : Login via Facebook
3. Google Play : Login via Google
4. Play9388 Play : Login via Play9388 Email
5. Player Play : System automatically figure out the type of login by using the corresponding Play verification mode. This method is used for configuring the game server and check whether a user acocunt exists or not. 

##Fast Play##

```

playSDK.api().doFastPlay(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Facebook Play##

```

playSDK.api().doFacebookPlay(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Google Play##

```

playSDK.api().doGooglePlay(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Play9388 Play##

```

playSDK.api().doPlay9388Play(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Player Play##

```

playSDK.api().doPlayerPlay(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```