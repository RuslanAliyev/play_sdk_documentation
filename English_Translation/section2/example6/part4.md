#Other Settings#

##Obtaining information regarding SDK's interactive hover button's settings##

Before making the PlaySDK's interactive hover button appear, must let API obtain the user's corresponding account-opening settings and SDK permissions.

```

playSDK.api().doGameControl(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Retrieving the product catalogue stored at the back-end##

```

playSDK.api().doProductID(Activity, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Executing Google payment program##

```
playSDK.api().doInAppBilling(Activity, appsorderid, receipt, signature, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Registering Google Cloud Messaging ID to SDK back-end##

```

playSDK.api().doPush(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##Login##

```

playSDK.api().doLogout(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    Failure scenario
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    Success scenario
  }
});

```

##SDK login page##

Start up the login page

```

playSDK.api().startLoginActivity(Activity);

```

##Login page's callback mechanism##

```

// PlaySDK's login page's callback mechanism
playSDK.api().onLoginActivityResult(this, requestCode, resultCode, data,
new ResponseObserver() {

  @Override
  public void onError(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // Handle server-connection failures
    // [For example: restart the login page]
    playSDK.api().startLoginActivity(LoginActivity.this);
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    if (responseClient.getEvent_code() == EventCode.EVENT_LOGIN_ERROR) {
      // Handle scenarios of abnormally leaving login page, for example: when user pressing the Back button or the program abnormally ending the login page.
      // [For example: restart the login page anew.]
      playSDK.api().startLoginActivity(LoginActivity.this);
    } else {
      // Handle scenarios of of normally leaving login page
      // [Usually this leads users to the game selection page]

      // Select game server
      playSDK.config().setServerID("1");

      // User clicks on his/her chosen game 
      playSDK.api().doPlayerPlay(new ResponseObserver() {

        @Override
        public void onError(ResponseClient responseClient, int event) {
          // TODO Auto-generated method stub
          // Failure scenario
        }

        @Override
        public void onSuccess(ResponseClient responseClient, int event) {
          // TODO Auto-generated method stub
          if (responseClient.isSuccess()) {
            // Success scenario
            // [Code here can be something like entering the game]
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
          } else {
            // Failure scenario
          }
        }
        });
      }
    }
  });
  
```

SDK's interactive hover button

> Have to start the service before starting the interactive hover button

Starting the interactive hover button service:

```
playSDK.api().startSDKViewService(Activity);

```

Starting the interactive hover button:

```

playSDK.api().openSDKView(Activity);

```

Closing the interactive hover button:

```

playSDK.api().closeSDKView(Activity);

```

Closing the interactive hover button service:

```

playSDK.api().stopSDKViewService(Activity);

```

Convert data from older versions:

> The previous version SDK9388's information is now PlaySDK's information.

```

playSDK.api().doConvertOlderData();

```

Check the machine for the existance of a Google account

```

playSDK.api().checkGoogleAccount();

```