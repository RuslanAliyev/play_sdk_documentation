#PlaySDK API's Documentation#

- [config](example1.md)
- [player](example2.md)
- [host](example3.md)
- [purchaseControl](example4.md)
- [gcm](example5.md)
- [api](example6/README.md)
	- [Register](example6/part1.md)
	- [Login](example6/part2.md)
	- [Play](example6/part3.md)
	- [Other](example6/part4.md)	