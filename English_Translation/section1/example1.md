#2014-12-21 Release Play SDK Version 1.7.5's usage guide#

##About##

|  |  |
| -- | -- |
| API's name | Play SDK |
| Version | 1.7.5 |
| Release date | 2014-Dec-21 |
| Developer | Neo Hsu |

##Features##

- Play SDK Login page

- Play SDK Hover button

- Play SDK Hover page

- Payment via Google

- Google Cloud Messaging for Play SDK

##Play SDK API's Overview##

- **Your game will use:**

	- PlaySDK

	- **PlaySDK will use:**
	
		- Facebook SDK
		
		- Google Play Services Library
		
		- android-support-v7-appcompat

##Using PlaySDK - Inside Android Manifest##

####Add in these permissions####

```

<manifest ...>

	<uses-permission android:name="android.permission.INTERNET" />
	<uses-permission android:name="android.permission.GET_ACCOUNTS" />
	<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
	<uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" />
	<uses-permission android:name="android.permission.WAKE_LOCK" />
	<uses-permission android:name="android.permission.VIBRATE" />
	<uses-permission android:name="com.android.vending.BILLING" />

	...
	
</manifest>	
	
```	

####Also add in these 2 permissions####

They are needed by Google Cloud Messaging.

Remember to fill in the currently-used package name.

```

<manifest ...>

	...

	<uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />	
	<uses-permission android:name="`[ package name]`.permission.C2D_MESSAGE" />
	
	...
	
</manifest>	
	
```	
	
####Rename the application name to `com.forgame.playsdk.PlayApplication`####

```xml

<manifest ...>

	...

	<application android:name="com.forgame.playsdk.PlayApplication" ...>
	
	...
	
	</application>
		
</manifest>	
	
```	

####Add this in, to determine Google Play Service's version####

```

<manifest ...>

	...

	<application android:name="com.forgame.playsdk.PlayApplication" ...>
	
		<meta-data
		  android:name="com.google.android.gms.version"
		  android:value="@integer/google_play_services_version" />	
	
		...
	
	</application>
		
</manifest>	
	
```	

####Add this in, to setup Facebook SDK, which will be used to establish corresponding Facebook App ID####

```

<manifest ...>

	...

	<application android:name="com.forgame.playsdk.PlayApplication" ...>
        
		...
	
		<meta-data
		  android:name="com.facebook.sdk.ApplicationId"
		  android:value="@string/facebook_app_id" />        
        
		...
	
	</application>
		
</manifest>	
	
```	

####List your game application's activities####

Remember to fill in the names of your game app's package name, project name and page(s)' name.

```

<manifest ...>

	...

	<application android:name="com.forgame.playsdk.PlayApplication" ...>
        
		...
	
        <activity
            android:name="`[ package name]`.`[ your game project's name]`.`[ your game project's main page's name]`"
            android:label="@string/app_name" >
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>       

		<activity
            android:name="`[ package name]`.`[ your game project's name]`.`[ your game project's other page(s)' name]`"
            android:launchMode="singleTask"
            android:label="@string/app_name" >
        </activity>
		
		<!-- List all the other page(s)' in your game app here. e.g.: the page(s) after successful login -->
		<activity ... ></activity>		
		
		...
	
	</application>
		
</manifest>	
	
```	

####Add in these services####

```

<manifest ...>

	...

	<application android:name="com.forgame.playsdk.PlayApplication" ...>
        
		...
	
		<service
		  android:name="com.forgame.playsdk.util.service.PlaySDKCircleView"
		  android:exported="true" />
		<service
		  android:name="com.forgame.playsdk.util.service.AngelCircleView"
		  android:exported="true" />
		<service android:name="org.OpenUDID.OpenUDID_service" >
		  <intent-filter>
		    <action android:name="org.OpenUDID.GETUDID" />
		  </intent-filter>
		</service>
		
		...
	
	</application>
		
</manifest>	
	
```	

####Add in these - For receiving Google Cloud Messaging registrations####

Remember to fill in the currently-used package name and your game app's name

```

<manifest ...>

	...

	<application android:name="com.forgame.playsdk.PlayApplication" ...>
        
		...
		
		<receiver
		  android:name="com.forgame.playsdk.util.management.MyGCMBroadcastReceiver"
		  android:permission="com.google.android.c2dm.permission.SEND" >
		  <intent-filter>
		    <!-- Receives the actual messages. -->
		    <action android:name="com.google.android.c2dm.intent.RECEIVE" />
		    <action android:name="com.google.android.c2dm.intent.REGISTRATION" />		
		    <category android:name="`[currently-used package name]`.`[your game app's name]`." />
		  </intent-filter>
		</receiver>
		
		...
	
	</application>
		
</manifest>	
	
```	

####Add in these - For starting Google Cloud Messaging service####

```

<manifest ...>

	...

	<application android:name="com.forgame.playsdk.PlayApplication" ...>
        
		...
		
		<service android:name="com.forgame.playsdk.util.service.GcmIntentService" />
	
	</application>
		
</manifest>	
	
```	


##Using PlaySDK - Inside Value/String.xml##

####Add this entry, fill in the Facebook App ID needed by Facebook SDK####

```

<string name="facebook_app_id">Facebook App ID</string>

```	



##Using PlaySDK - Inside your game app’s main page's Java file##

####Initialize PlaySDK, Connect to server####

```

public class `[name of your game app's main page]` {

	PlaySDK playSDK;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.`[the corresponding view xml ]`);
        playSDK =  PlaySDK.getInstance();
		...
    }

    public void onResume(){
    	super.onResume();

    	if (playSDK == null) {
    	  playSDK = PlaySDK.getInstance();
    	}

    	playSDK.config().isTestEnvironment(true);

    	//With or without log messages
    	playSDK.config().hasDisplayLog(true);

    	// Connect to server. 1st parameter is the actual platform. The 2nd parameter is the testing platform
    	playSDK.config().setConnectDomain("http://connect.9388.com/app", "http://connect-t.9388.com/app");

    	// set the payment server’s address. 1st parameter is the actual platform. The 2nd parameter is the testing platform
    	playSDK.config().setPaymentDomain("http://payment.9388.com/app", "http://payment-t.9388.com/app");

    	// Set public key to connect to server
    	playSDK.config().setConnectPublicKey("kd9s@ls^vm");

    	// Set public key to connect to the payment server
    	playSDK.config().setPayPublicKey("ckfg4#xkq7");

    	// set the game’s name. This will be the ‘Game Tag’ that the server receives
    	playSDK.config().setGame("something");

    	// set game’s version number
    	playSDK.config().setGameVersion("1.0.0");

    	// True means that the interactive hover button will fade in transparency after 2s of appearing. False means vice versa
    	playSDK.config().setSDKFadeOut(false);

    	// set page orientation: true means horizontal
    	playSDK.config().setSDKLandscape(true);

    	// set the icon displayed on the alert window
    	playSDK.config().setDisplayGameIcon(R.drawable.ic_launcher);

    	// set the title displayed on the alert window
    	playSDK.config().setDisplayGameName("Title");

    	// Set the page where users will be directed to upon clicking Google Cloud Messaging’s notifications. This is usually the main page of your application
    	playSDK.config().setGCMIntent("`[ package name]`.`[ your game project's name]`.`[ your game project's main page's name]`");

    	// Set whether or not to persist prepaid users' ability to get their third party credentials when they enter a game. (true: persist their credentials when they enter game/false:start them off based on SDK's information)
    	playSDK.config().setThirdPartyPay(false);

    	// Set Google payment key
    	playSDK.config().setPurchasePublickey("Google payment key");

    	// Set the x position of the interactive hover button
    	playSDK.config().setCircleX(50);

    	// Set the y position of the interactive hover button
    	playSDK.config().setCircleY(50);
    }
	
	...
	
}

```

####Handle login####

PlaySDK provides a login activity for users to login before playing games.
Therefore the method that handles the login event (e.g.: an Override onClick method for a login button) should have this:

```

public class `[name of your game app's main page]` {

	...
	
    @Override
	public void onClick(View v) {

		...
		
		playSDK.api().startLoginActivity(`[ your game project's main page's name]`.this);
		
		...
		
	}
	
	...
	
}

```

####Callback processing after PlaySDK’s login page####

Make onActivityResult method like so:

```

public class `[name of your game app's main page]` {

	...
	
    @Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
	
		super.onActivityResult(arg0, arg1, arg2);	
		
		if(playSDK == null){			
			playSDK = PlaySDK.getInstance();
		}  
 
		// ResponseObserver is passed upon every login. It contains login success or failure information
		playSDK.api().onLoginActivityResult(this, arg0, arg1, arg2, new ResponseObserver(){

			@Override
				public void onError(ResponseClient responseClient, int event) {
				
				// if connection error, then restart login page
				playSDK.api().startLoginActivity(`[ your game project's main page's name]`.this);
			}

			@Override
			public void onSuccess(ResponseClient responseClient, int event) {

				if (responseClient.getEvent_code() == EventCode.EVENT_LOGIN_ERROR) {
					//If login error, write the code here to handle login exception (for example, divert user away from the login page to a login-failed page), and then restart login page
					playSDK.api().startLoginActivity(MainActivity.this);
				} 
				else {
					//write code to process a normal successful login here, 
					//then write code that does something like allowing the user to choose a game server
					playSDK.config().setServerID("1");

					// When users chose a game, the code here handles it
					playSDK.api().doPlayerPlay(new ResponseObserver() {

						@Override
						public void onError(ResponseClient responseClient, int event) {
							//code here handles events of failure
						}

						@Override
						public void onSuccess(ResponseClient responseClient, int event) {
							if (responseClient.isSuccess()) {
								//code here handles events of failure, then, eg, set up game
								Intent intent = new Intent(`[ your game project's main page's name]`.this, `[ your game project's after-login-page's name]`.class);
								startActivity(intent);
								finish();
							} 
							else {
								//code here handles events of failure
							}
						}
					});
				}
			}
		});		
	}

	...
	
}

```

####The Java codes for the page(s) after successful login####	

From here is where you see after you logged in, with an interactive hover button.

From here is where the user pays then play games.

The page(s) after login success should be like this:

```

public class `[ the page after successful login ]`{

	PlaySDK playSDK;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout. `[ corresponding layout xml ]`);
		
		playSDK = PlaySDK.getInstance();
		
		// starting up the service in the SDK’s interactive hover button
		playSDK.api().startSDKViewService(this);
		
		// initialize the payment mechanisms when the payment page loads
		playSDK.purchaseControl().init(`[ the page after successful login ]`.this);

		// Obtaining PlaySDK's interactive hover button's control setting information
		playSDK.api().doGameControl(new ResponseObserver() {

			  @Override
			  public void onError(ResponseClient responseClient, int event) {
			    // failure
			  }
	
			  @Override
			  public void onSuccess(ResponseClient responseClient, int event) {
			    // success
			    playSDK.api().openSDKView(`[ the page after successful login ]`.this);
			  }
		});
				
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		// Starting up SDK’s interactive hover button
		playSDK.api().openSDKView(`[ the page after successful login ]`.this);
				
		// Register with Google Cloud Messaging. Can only process settings after login, otherwise it’ll be unable to match user to RegisterID
		playSDK.gcm().doGCMRegister();
		
	}
	
	...

}

```

####If a user is going to pay and then start game####	

The following code handles scenario of a user paying then playing:

```

// Directed to Google’s payment gateway after clicking on a product
playSDK.purchaseControl().purchaseFlow(this, "Product-ID", new PurchaseResponseObserver() {
	  @Override
	  public void onError(String msg, int event) {
		// handle payment failure
	  }

	  @Override
	  public void onSuccess(String msg, int event) {
		// handle payment success
	  }

});

```

####Paying####

```

public class `[ the page after successful login ]`{

	...
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		//In the payment page’s Java activity file, need to re-write the onActivityResult method in regards to the Google callback mechanism for the event of successful purchase.
		//set up the callback mechanism of the payment mechanism.
		
		if (!playSDK.purchaseControl().getHasActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		} else {

		}		
	}
	
	...
	
}

```

####Go to the page for prepaid third-parties directly and start playing games####	

The following code handles such scenarios:

```

//Go to the page for prepaid third-parties directly
//To persist prepaid users' ability to get their third party credentials when they enter a game, then do the following:
playSDK.config().setThirdPartyPay(true);

// Opening up the page for prepaid users
Intent intent = new Intent(this, com.forgame.playsdk.activity.PlaySDKActivity.class);
Bundle bundle = new Bundle();
bundle.putString("url", playSDK.host().getPayUrl());
intent.putExtras(bundle);
intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
startActivity(intent);

```

####Closing####	

```

public class `[ the page after successful login ]`{

	...
	
	@Override
	protected void onPause() {
		super.onPause();
		playSDK.api().closeSDKView(this);
	}

	// for closing the payment page
	@Override
	protected void onDestroy() {
		super.onDestroy();
		playSDK.api().stopSDKViewService(this);
		
		// stopping the payment mechanism
		playSDK.purchaseControl().dispose();
	}
}

```


##Play SDK API's Detailed Overview##	

Your app: Your game application's main page's Java file
-	onCreate method
-	- PlaySDK.getInstance calls the singleton constructor of the PlaySDK API’s PlaySDK.java file.

PlaySDK API: PlaySDK.java
-	Initiates just one PlaySDK instance
-	Also initiates just one API.java instance

Your app: Your game application's main page's Java fie
-	When someone logs in, inside the method that handles this login (e.g.: onClick method), there should be this method: `startLoginActivity(NAME_OF_YOUR_APP'S_MAIN_PAGE'S ACTIVITY)`, which calls the startLoginActivity method in the PlaySDK API’s API.java file.

PlaySDK API’s API.java
-	startLoginActivity method starts up PlaySDK API’s LoginActivity.java (which’s associated with activity_login.xml view) . Through this view the user can sign up or log in.
-	startLoginActivity method also calls back to your game application's main page's Java file's onLoginActivityResult method.

Your game application's main page's Java file's onLoginActivityResult method:
-	If sign up/login is successful:
-	- Chose game server (Set Server ID): `playSDK.config () setServerID ("Server ID");`
-	- Enter game: `playSDK.api () doPlayerPlay (new ResponseObserver () {....}).;` 
-	- then user gets directed into: Your game application (Your game application's page after successful login)

PlaySDK API’s API.java
-	Runs Service.PlaySDKCircleView.java (which includes the SetSDKView method, which is responsible for making the interactive hover button appear)

Your game application (Your game application's page after successful login):
This page contains the pay button, enter game button and the interactive hover button

-	onCreate method
-	- doGameControl method
-	- - success
-	- - - openSDKView method calls PlaySDK API’s Service.PlaySDKCircleView.java’s SetSDKView method, which brings up the playsdk_circle_view.xml (which is the interactive hover button)

-	onClick method

-	- if enter game button is clicked, PlaySDK API’s PlaySDKActivity.java’s onResume method will be invoked, with the url for enter game passed in, which brings up activity_playsdk.xml page with the url for play game inside.

-	- if the menu associated with the interactive hover button is pressed, PlaySDK API’s PlaySDKActivity.java’s onResume method will be invoked, with the another url passed in, which brings up activity_playsdk.xml page with another url.
	
	
##Recap##
	
####ResponseObserver####	

All the API callback are handled by by ResponseObserver.

The ResponseClient passed back by the ResponseObserver indicates the API processing status.

```

playSDK.api().doPlayerPlay(new ResponseObserver() {

	@Override
	public void onError(ResponseClient responseClient, int event) {
		// handling all failure scenarios here
	}

	@Override
	public void onSuccess(ResponseClient responseClient, int event) {
		// handling all success scenarios here 
	}
});

```
	
####ResponseClient####

###### Public ######	

| Method | Return | Description |
| -- | -- | -- |
| isSuccess | Boolean | Success or not |
| getCode_number | Int | Get success or error code |
| getEvent_code | Int | Get event code |
| getEvent_name | String | Get event name |
| getError_message | String | Get error info (nothing will be returned if success) |
| getData | String | Get returned data in case of success |
| getResponse_timestamp | String | Get the time of completion |
| getProducts_id | ArrayList | Product list, only when getProductID API succeeds will there be an returned parameter |
| displayInfo(isSuccess, title) | void | Using Log to bring out all the info inside ResponseClient. isSuccess: true/false. title: prefix of the displayed |


##API Glossary##	
	
- doFastRegister (ResponseObserver responseObserver)
- doFastLogin (ResponseObserver responseObserver)
- doFastPlay (ResponseObserver responseObserver)
- doFastBindFacebook (Activity activity, ResponseObserver responseObserver)
- doFastBindGoogle (ConnectionResult mConnectionResult, GoogleApiClient mGoogleApiClient, Activity activity, ResponseObserver + responseObserver)
- doFastBindPlay9388 (String result, ResponseObserver responseObserver)
- doFacebookRegister (Activity activity, ResponseObserver responseObserver)
- doFacebookLogin (ResponseObserver responseObserver)
- doFacebookPlay (ResponseObserver responseObserver)
- doGoogleRegister (ConnectionResult mConnectionResult, GoogleApiClient mGoogleApiClient, Activity activity, ResponseObserver + responseObserver)
- doGoogleLogin (ResponseObserver responseObserver)
- doGooglePlay (ResponseObserver responseObserver)
- doPlay9388Register (String result, ResponseObserver responseObserver)
- doPlay9388Login (ResponseObserver responseObserver)
- doPlay9388Play (ResponseObserver responseObserver)
- doGameControl (ResponseObserver responseObserver)
- doProductID (ResponseObserver responseObserver)
- doLogout (Activity activity)
- startLoginActivity (Activity activity)
- onLoginActivityResult (Activity activity, int requestCode, int resultCode, Intent data, ResponseObserver responseObserver)
