# Summary

Documentation for: Release Play SDK Version 1.7.5 (2014-12-21)

* [PlaySDK's Documentation](section1/README.md)
    * [Play SDK v1.7.5](section1/example1.md)
* [PlaySDK API's Documentation](section2/README.md)
    * [config](section2/example1.md)
    * [player](section2/example2.md)
	* [host](section2/example3.md)	
    * [purchaseControl](section2/example4.md)
    * [gcm](section2/example5.md)
	* [api](section2/example6/README.md)	
		* [Register](section2/example6/part1.md)	
		* [Login](section2/example6/part2.md)
		* [Play](section2/example6/part3.md)
		* [Other](section2/example6/part4.md)	