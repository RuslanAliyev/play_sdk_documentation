#Introduction#

This document details this API's specifications, provides reference and uniformity-guidance to vendors.

Author: Ruslan Aliyev 	

Copyright c 2014 Forgame.com. All right reserved.

- [PlaySDK's Documentation](section1/README.md)
- [PlaySDK API's Documentation](section2/README.md)